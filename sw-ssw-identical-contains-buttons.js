// ==UserScript==
// @name         SW/SSW Identical/Contains One-Click Toggle
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  get rid of the dumb select in the SW/SSW and replace with some nice toggles instead
// @author       Snack
// @match        https://www.neopets.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

function attachStyles() {
    const styleNode = document.createElement('style');
    styleNode.type = 'text/css';
    const styles = `
        .ssw-button-wrapper {
            display: flex;
            flex-flow: row nowrap;
            gap: 10px;
        }
        .ssw-button-custom {
            height: 40px;
            flex-grow: 1;
            font-family: "MuseoSansRounded700", 'Arial', sans-serif;
            box-shadow: none !important;
            cursor: pointer;
        }
        .ssw-button-custom:hover {
            color:#fff;
            text-shadow: 0 0 4px #000;
	        background: #59c2ff;
	        background: linear-gradient(#59c2ff,#195de0);
            box-shadow: none !important;
        }
        .ssw-button-custom:focus {
            background: linear-gradient(#539ff3,#2556ba);
        }
        .ssw-button-selected,
        .ssw-button-custom:focus.ssw-button-selected,
        .ssw-button-custom:hover.ssw-button-selected {
            background: linear-gradient(#195de0,#59c2ff);
        }
        .ssw-filters__2020 select, .wizard-filters select {
            display: none;
        }
    `;
    if (styleNode.styleSheet) styleNode.styleSheet.cssText = styles;
    else styleNode.appendChild(document.createTextNode(styles));
    document.getElementsByTagName("head")[0].appendChild(styleNode);
}

function createAndAttachToggles(criteriaSelector, filtersSelector) {
    const identicalButton = document.createElement('button');
    identicalButton.innerText = 'Identical';
    identicalButton.classList.add('button-blue__2020', 'ssw-button-custom', 'ssw-button-selected');
    const containsButton = document.createElement('button');
    containsButton.innerText = 'Contains';
    containsButton.classList.add('button-blue__2020', 'ssw-button-custom');
    identicalButton.addEventListener('click', () => {
        const originalSelect = document.querySelector(criteriaSelector);
        if (originalSelect.value !== 'exact') {
            identicalButton.classList.add('ssw-button-selected');
            containsButton.classList.remove('ssw-button-selected');
            originalSelect.value = 'exact';
        }
    });
    containsButton.addEventListener('click', () => {
        const originalSelect = document.querySelector(criteriaSelector);
        if (originalSelect.value !== 'containing') {
            identicalButton.classList.remove('ssw-button-selected');
            containsButton.classList.add('ssw-button-selected');
            originalSelect.value = 'containing';
        }
    });
    const wrapper = document.createElement('div');
    wrapper.classList.add('ssw-button-wrapper');
    wrapper.appendChild(identicalButton);
    wrapper.appendChild(containsButton);
    document.querySelector(filtersSelector).appendChild(wrapper);
}

function createAndAttachSSWToggles() {
    createAndAttachToggles('#ssw-criteria', '.ssw-filters__2020');
}

function createAndAttachSWToggles() {
    createAndAttachToggles('.wizard-filters #criteria', '.wizard-filters');
}

(function() {
    attachStyles();
    if (document.querySelector('.navsub-ssw-icon__2020')) {
        createAndAttachSSWToggles();
    }

    if (document.querySelector('#shopWizardFormResults')) {
        createAndAttachSWToggles();
    }
})();