// ==UserScript==
// @name         Sent Neomail Tracker
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Track sent Neomails
// @author       Snack
// @match        https://www.neopets.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

let parser;

// For debugging
function createDummyNeomail() {
    return {
        id: '3784023143',
        timestamp: +Date.now(),
        un: 'drracket',
        subject: 'Hey, this is a dummy neomail!',
        message: 'Hey, whats up! This is a fake message lol...',
    };
}

// For debugging
async function loadDummyData() {
    await GM_setValue('saved-neomails', JSON.stringify([...Array(21).keys()].map(createDummyNeomail)));
}

async function getSavedNeomails() {
    return JSON.parse(await GM_getValue('saved-neomails', '[]'));
}

async function saveNeomails(neomails) {
    await GM_setValue('saved-neomails', JSON.stringify(neomails));
}

async function deleteNeomails(toDelete) {
    let neomails = await getSavedNeomails();
    neomails = neomails.filter(({ id }) => !toDelete.includes(id));
    await saveNeomails(neomails);
}

async function addNewNeomail(neomail) {
    let neomails = [
        {
            ...neomail,
            id: `${Math.floor((Math.random() * 10000000000) + 1)}`,
            timestamp: +Date.now(),
        },
        ...await getSavedNeomails(),
    ];
    await GM_setValue('saved-neomails', JSON.stringify(neomails));
}

function getNeomailById(neomails, neomailId) {
    return neomails.find(({ id }) => id === neomailId);
}

function getHigher(neomails, lower) {
   const higher = Math.min(neomails.length, lower + 20);
   return lower > higher ? lower - 1 : neomails.slice(lower, higher).length + lower - 1;
}

function getLower() {
    const lowerIndex = location.search.split(/[?&]/g).findIndex((item) => item.indexOf('lower') !== -1);
    return lowerIndex === -1
        ? 0
        : parseInt(location.search.split(/[?&]/g)[lowerIndex].split('=')[1], 10);
}

function createWarning() {
    const str = `
        <b style="color:#FF0000;">Anyone found to be breaking our <a href="/chatrules.phtml" target="_blank">rules</a> will have their account permanently frozen.</b>
        If you spot anything that isn't right and our monitors haven't addressed the problem, please notify us using <a href="/autoform_abuse.phtml?abuse=report"><b>this form</b></a>
        and it will be removed ASAP.
        <br><br>
    `;
	const doc = parser.parseFromString(str, 'text/html');
	return doc.body.innerHTML;
}

function createTopSection() {
    const str = `
        <br>
        <b>Note:</b> When you have more than <b>100</b> messages in your Inbox, you cannot receive Neomail!
        <br><br>
        <b style="color:#FF0000;">Anyone found to be breaking our <a href="/chatrules.phtml" target="_blank">rules</a> will have their account permanently frozen.</b>
        If you spot anything that isn't right and our monitors haven't addressed the problem, please notify us using <a href="/autoform_abuse.phtml?abuse=report"><b>this form</b></a>
        and it will be removed ASAP.<br><br> If you could help us by deleting old messages, we would really appreciate it!
        <br><br>
    `;
	const doc = parser.parseFromString(str, 'text/html');
	return doc.body.innerHTML;
}

function getTableHeader(neomails, lower) {
    const higher = getHigher(neomails, lower);
    const total = neomails.length;
    const str = `<div align="center"><b>Viewing:</b> Sent | <b>Messages:</b> ${lower + 1} - ${higher + 1} of ${total}</div><br>`;
    const doc = parser.parseFromString(str, 'text/html');
	return doc.body.innerHTML;
}

function formatTimestamp(timestamp) {
    const d = new Date(timestamp);
    return d
        .toLocaleString('en-GB', { timezone: 'PST', hour12: true })
        .replace(',', '')
        .replace(/:\d\d (.+)$/g, '$1')
        .replace(/ (\d):/g, ' 0$1:');
}

function createNeomail(neomail, isEven) {
    const bgColor = isEven ? '#FFFFFF' : '#EDEDED';
    return `
        <tr>
		    <td bgcolor="${bgColor}" align="center" class="medText" style="border-bottom: 1px solid #000000;">
                <input class="sent-checkbox" type="checkbox" name="checkbox_arr[]" value="${neomail.id}">
            </td>
			<td bgcolor="${bgColor}" class="medText" style="border-bottom: 1px solid #000000;">
                ${formatTimestamp(neomail.timestamp)}
            </td>
            <td bgcolor="${bgColor}" class="medText" style="border-bottom: 1px solid #000000;">
                <span style="font-weight: normal; font-style: italic;">${neomail.un}</span>
                <br>
                [<a href="/userlookup.phtml?user=${neomail.un}"><b>${neomail.un}</b></a>]
            </td>
            <td bgcolor="${bgColor}" style="border-bottom: 1px solid #000000; font-size: 9pt;">
                <a href="neomessages.phtml?type=read_sent_message&amp;folder=Sent&amp;id=${neomail.id}" style="font-size: 9pt; font-weight: normal;">
                    ${neomail.subject}
                </a>
            </td>
            <td align="right" bgcolor="${bgColor}" class="medText" style="border-bottom: 1px solid #000000;">Sent</td>
        </tr>
    `;
}

async function deleteMessages() {
    let toDelete = [];
    document.querySelectorAll('.sent-checkbox').forEach((checkbox) => {
        if (checkbox.checked) {
            toDelete.push(`${checkbox.value}`);
        }
    });
    if (toDelete.length > 0) {
        await deleteNeomails(toDelete);
        window.location.reload();
    }
}

function createTable(neomails) {
    const tableStr = `
        <table width="100%" align="center" cellpadding="3" cellspacing="0" border="0" style="border: 1px solid #000000">
			<tbody>
            <tr>
			    <td width="20" class="contentModuleHeaderAlt" style="border-bottom: 1px solid #000000">&nbsp;</td>
			    <td width="130" class="contentModuleHeaderAlt" style="border-bottom: 1px solid #000000;"><b>Date Sent</b></td>
			    <td width="200" class="contentModuleHeaderAlt" style="border-bottom: 1px solid #000000;"><b>Sent To</b></td>
			    <td class="contentModuleHeaderAlt" style="border-bottom: 1px solid #000000;"><b>Subject</b></td>
			    <td width="10" class="contentModuleHeaderAlt" style="border-bottom: 1px solid #000000; white-space:nowrap"><b>Status</b></td>
			</tr>
            ${neomails.reduce((acc, nm, i) => acc + createNeomail(nm, i % 2 === 0), '')}
            <tr>
            <td colspan="2" bgcolor="#DEDEDE" align="left" valign="middle">
				<input type="checkbox" id="check-all-selected"> Check All
			</td>
            <td colspan="3" bgcolor="#DEDEDE" align="right">
                <button id="delete-sent-selected" value="Delete all selected!">Delete all selected!</button>
            </td>
            </tr>
            </tbody>
        </table>
    `;
    const table = parser.parseFromString(tableStr, 'text/html');
    return table.body.innerHTML;
};

function createPagination(neomails) {
    if (neomails.length > 20) {
        const lower = getLower();
        let previous = '';
        let next = '';
        if (lower >= 20) {
            previous = `<a href="/neomessages.phtml?lower=${lower - 20}&amp;type=sent"><b><span class="pointer">«</span> Previous 20</b></a>`;
        }
        if (lower + 20 < neomails.length) {
            next = `<a href="/neomessages.phtml?lower=${lower + 20}&amp;type=sent"><b>Next 20 <span class="pointer">»</span></b></a>`;
        }
        const str = `
            <table width="100%" cellpadding="2" cellspacing="0" border="0">
			    <tbody><tr>
			    	<td align="left" class="medText">${previous}</td>
			    	<td align="right" class="medText">${next}</td>
			    </tr>
		    </tbody></table>
        `;
        return parser.parseFromString(str, 'text/html').body.innerHTML;
    }
}

function createNoMessages() {
    const str = `
        <div align="center">
            <hr noshade="" size="1" color="#E4E4E4"><br>
			<b>You don't have any saved messages yet.</b><br>
			<hr noshade="" size="1" color="#E4E4E4"><br>
            <p></p>
            <p></p>
            <br clear="all">
		</div>
    `;
    return parser.parseFromString(str, 'text/html').body.innerHTML;
}

async function createListPage() {
    const neomails = await getSavedNeomails();
    const topSection = createTopSection();
    const lower = getLower();
    document.querySelector('.content').innerHTML += topSection;
    document.querySelector('.content').innerHTML += getTableHeader(neomails, lower);
    const pagination = createPagination(neomails);
    if (pagination) {
        document.querySelector('.content').innerHTML += pagination;
    }

    const slicedNeomails = neomails.slice(lower, getHigher(neomails, lower) + 1);

    if (slicedNeomails.length === 0) {
        document.querySelector('.content').innerHTML += createNoMessages();
    } else {
        document.querySelector('.content').innerHTML += createTable(slicedNeomails);
        if (pagination) {
            document.querySelector('.content').innerHTML += pagination;
        }
        document.querySelector('.content').innerHTML += '<br><br><br><br>';
        setTimeout(() => {
            document.querySelector('#check-all-selected').addEventListener('click', () => {
                if (document.querySelector('#check-all-selected').checked) {
                    document.querySelectorAll('.sent-checkbox').forEach((checkbox) => {
                        checkbox.checked = true;
                    });
                } else {
                    document.querySelectorAll('.sent-checkbox').forEach((checkbox) => {
                        checkbox.checked = false;
                    });
                }
            });
            document.getElementById('delete-sent-selected').addEventListener('click', async () => {
                await deleteMessages();
            });
        }, 0);
    }

}

function createSingleNeomail(neomail) {
    const message = neomail.message.replace(/\n/g, '<br />');
    const str = `
        <table width="100%" cellpadding="6" cellspacing="1" border="0" style="border: 1px solid #000000;"><tbody>
            <tr>
		        <td width="100" bgcolor="#C8E3FF" class="medText">
			        <b>To:</b>
			    </td>
			    <td bgcolor="#F6F6F6" class="medText"><img src="//images.neopets.com/neoboards/avatars/default.gif" width="50" height="50" alt="" border="0" align="left" style="padding-right: 4px;"><br>[<a href="/userlookup.phtml?user=${neomail.un}"><b>${neomail.un}</b></a>] <span style="font-weight: normal; font-style: italic;">${neomail.un}</span><br><a href="/neofriend_requests.phtml?neofriend_add=${neomail.un}"><span class="pointer">»</span> Make this user your Neofriend</a></td>
			</tr>
            <tr>
				<td bgcolor="#C8E3FF" class="medText">
					<b>Sent:</b>
				</td>
				<td bgcolor="#F6F6F6" class="medText">${formatTimestamp(neomail.timestamp)}</td>
			</tr>
            <tr>
				<td bgcolor="#C8E3FF" class="medText">
					<b>Folder:</b>
				</td>
				<td bgcolor="#F6F6F6" class="medText" <a="" href="/neomessages.phtml?folder=Inbox"><b>Sent</b></td>
			</tr>
            <tr>
				<td bgcolor="#C8E3FF" class="medText">
					<b>Subject:</b>
				</td>
				<td bgcolor="#F6F6F6">${neomail.subject}</td>
			</tr>
            <tr>
				<td bgcolor="#C8E3FF" valign="top" class="medText" nowrap="">
                    <b>Message:</b>
			    </td>
			    <td bgcolor="#FFFFFF" valign="top">${message}</td>
			</tr>
			</tbody>
        </table>
    `;
    return parser.parseFromString(str, 'text/html').body.innerHTML;
}

async function createNeomailPage() {
    const neomails = await getSavedNeomails();
    const neomailIdIndex = location.search.split(/[?&]/g).findIndex((item) => item.indexOf('id') !== -1);
    const neomailId = location.search.split(/[?&]/g)[neomailIdIndex].split('=')[1];
    const topDivs = document.querySelectorAll('.content div[align="center"]');
    topDivs[2].remove();
    topDivs[1].remove();
    const topSection = createWarning();
    const singleNeomailObj = getNeomailById(neomails, neomailId);
    const singleNeomail = createSingleNeomail(singleNeomailObj);
    document.querySelector('.content').innerHTML += topSection;
    document.querySelector('.content').innerHTML += singleNeomail;
};

function isNeomailPage() {
    return location.search.indexOf('read_sent_message') !== -1 && location.search.indexOf('Sent') !== -1;
}

function isListPage() {
    return location.search.indexOf('read_sent_message') === -1 && location.search.indexOf('sent') !== -1;
}

function createAndAttachNavItem() {
    if (document.querySelector('a[href="/neomessages.phtml?folder=Inbox"]')) {
        const str = `<div style="display:inline-block">&nbsp;| <a href="/neomessages.phtml?type=sent">Sent</a></div>`;
        const html = parser.parseFromString(str, 'text/html').body.querySelector('div');
        const sibling = document.querySelector('a[href="/neomessages.phtml?type=send"]');
        sibling.parentNode.insertBefore(html, sibling.nextSibling);
    }
}

function handleSend() {
    const sendButton = document.querySelector('input[value="Send a NeoMail™"]');
    if (sendButton) {
        const onClick = sendButton.onclick;
        sendButton.onclick = null;
        sendButton.addEventListener('click', async (e) => {
            e.preventDefault();
            const un = document.querySelector('input[name="recipient"]').value;
            const subject = document.querySelector('input[name="subject"]').value;
            const message = document.querySelector('textarea[name="message_body"]').value;
            onClick();
            if (document.neomessage.recipient.value.indexOf("'") === -1) {
                await addNewNeomail({ un, subject, message });
                document.querySelector('form[name="neomessage"]').submit();
            }
        });
    }
}

async function revokeLatestSentMaybe() {
    if (window.location.href.indexOf('process_neomessages') !== -1 && document.querySelector('.errormess')) {
        const neomails = await getSavedNeomails();
        const lastTimestamp = neomails[0].timestamp;

        // Only revoke mail sent within last 30 seconds
        // so that we don't delete an old one if
        // a user refreshed the page or something
        if (+Date.now() - lastTimestamp < 30000) {
            neomails.shift();
            await saveNeomails(neomails);
        }
    }
}

(async function() {
    if (window.location.href.indexOf('neomessages') !== -1) {
        //await loadDummyData();
        parser = new DOMParser();
        createAndAttachNavItem();
        await revokeLatestSentMaybe();
        handleSend();
        if (isNeomailPage()) {
            await createNeomailPage();
        } else if (isListPage()) {
            await createListPage();
        }
    }
})();