# Neopets Code Snippets

Tampermonkey code snippets for Neopets. Do what you want with them!

**USE THESE SCRIPTS AT YOUR OWN RISK**

## Active Pet Switcher

Puts a pet switcher into the beta layout topbar:

![Active Pet Switcher Demo](https://i.imgur.com/cgwp6z5.gif)

To load the pets into the select, you'll need to visit [the homepage](https://www.neopets.com/index.phtml). Every time you add or remove a pet, visit the homepage again to update the list of pets.

## Shop Page Total Value

Shows you the sum of the prices of all items on the page you're currently viewing in your shop:

![Shop Page Total Value Screenshot](https://i.imgur.com/HSLgVbU.png)

Note that this is calculated using *the prices you set in your shop*, not the prices on Jellyneo.

## Fix SSW on Explore Pages

Ever wondered why you can't use the SSW when on a page with a map? Use this to fix that.

![Fix SSW on Explore Pages Demo](https://i.imgur.com/9mehOtF.mp4)

## SW/SSW Identical/Contains Buttons

Replaces the SW/SSW identical/contains select with button toggles, turning a 2-click into a 1-click.

![SSW Identical/Contains Buttons Demo](https://i.imgur.com/BnsD1PX.gif)

![SW Identical/Contains Buttons Screenshot](https://i.imgur.com/sgVOeqQ.png)

## SW Quick Link

Adds an SW quick link to the top left of the beta layout.

![SW Quick Link Screenshot](https://i.imgur.com/NhnHB81.png)

## Sent Neomail Tracker

Ever been annoyed that you can't keep track of your sent Neomails? Use this!

Supports pagination and deletion. Note that this only starts tracking your sent Neomails *after* you install the userscript.

![Sent Neomail Tracker Screenshot](https://i.imgur.com/hUon0Kn.png)
![Sent Neomail Tracker Screenshot](https://i.imgur.com/DATC88a.png)

## Coincidence SSW Fix

Ever noticed that you can't make more than one SSW request when doing The Coincidence? Use this script to fix the silly bug that stops you from searching for all items.