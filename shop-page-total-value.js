// ==UserScript==
// @name         Shop Total Value
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Totals the values of the shop page you're currently viewing
// @author       Snack
// @icon         https://www.google.com/s2/favicons?sz=64&domain=tampermonkey.net
// @grant        none
// @match        https://www.neopets.com/*
// ==/UserScript==

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

(function() {
    if (document.querySelector('form[action*="market.phtml"]')) {
        const totalValue = Array.from(document.querySelectorAll('input[name^="cost_"]').values()).reduce((acc, c) => parseInt(c.value) + parseInt(acc), 0);
        const newNode = document.createElement('p');
        newNode.innerHTML = `Value of all items on this page: <b>${numberWithCommas(totalValue)} NP</b>`;
        document.querySelector('#content .content center').appendChild(newNode);
    }
})();
