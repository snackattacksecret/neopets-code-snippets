// ==UserScript==
// @name         Fix Coincidence SSW
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Lets you submit more than one SSW request on The Coincidence without breaking
// @author       Snack
// @match        *://*.neopets.com/space/coincidence.phtml
// @icon         https://www.google.com/s2/favicons?sz=64&domain=neopets.com
// @grant        none
// ==/UserScript==

(function() {
    $(document).ajaxComplete(function(event, jqXHR, settings) {
        $('html').removeClass('busy');
    });
})();