// ==UserScript==
// @name         Fix SSW on Explore Pages
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Allows you to use SSW on Explore pages again
// @author       You
// @match        https://www.neopets.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

var $, jQuery;
$ = jQuery = window.jQuery;

(function() {
    const mouseDownEvents = $._data(document, 'events').mousedown;
    const mapH5Canvas = document.querySelector('canvas.mapH5');
    if (mouseDownEvents && mouseDownEvents.length > 0 && mapH5Canvas) {
        $(document).off('mousedown', mouseDownEvents.handler);
    }
})();
