// ==UserScript==
// @name         Add Normal SW Link
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  add the normal SW link to the top instead of having to click into the submenu
// @author       Snack
// @match        https://www.neopets.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

function attachStyles() {
    const styleNode = document.createElement('style');
    styleNode.type = 'text/css';
    const styles = `
        .sw-quick-link-icon {
            display: inline-block;
            width: 30px;
            height: 30px;
            background-image: url('https://images.neopets.com/themes/h5/basic/images/shopwizard-icon.png');
            background-size: 30px 30px;
            margin-left: 10px;
        }
    `;
    if (styleNode.styleSheet) styleNode.styleSheet.cssText = styles;
    else styleNode.appendChild(document.createTextNode(styles));
    document.getElementsByTagName("head")[0].appendChild(styleNode);
}

function createAndAttachIcon() {
    const swLink = document.createElement('a');
    swLink.href = 'https://www.neopets.com/shops/wizard.phtml';
    swLink.classList.add('sw-quick-link-icon');
    const leftIcons = document.querySelectorAll('.navsub-left__2020 *[class^="navsub"]');
    const lastLeftIcon = leftIcons[leftIcons.length - 1];
    lastLeftIcon.parentNode.insertBefore(swLink, lastLeftIcon.nextSibling);
}

(function() {
    if (document.querySelector('.nav-top-grid__2020')) {
        attachStyles();
        createAndAttachIcon();
    }
})();