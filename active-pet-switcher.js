// ==UserScript==
// @name         Active Pet Switcher
// @namespace    http://tampermonkey.net/
// @version      0.7
// @description  Switch between active pets in the topbar
// @match        https://www.neopets.com/*
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

async function savePets() {
    const pets = new Set();
    const names = new Set();
    document.querySelectorAll('.hp-carousel-pet-container').forEach((petWrapper) => {
        const namePlate = petWrapper.querySelector('.hp-carousel-nameplate');
        const name = namePlate.innerText;
        if (name !== 'Add a Neopet!' && !names.has(name)) {
            names.add(name);
            const sci = petWrapper.querySelector('.hp-carousel-pet').getAttribute('data-petimage').split('/')[4];
            pets.add({ name, sci });
        }
    });

    if (pets.size === 0) {
        return;
    }

    await GM.setValue('pet-switcher-entries', JSON.stringify(Array.from(pets).sort((a, b) => a.name.localeCompare(b.name))));
}

async function getPets() {
    return JSON.parse(await GM.getValue('pet-switcher-entries', '[]'));
}

async function handlePetChange(petName) {
    const pets = await getPets();
    const { sci } = pets.find(({ name }) => name === petName);
    await fetch(`https://www.neopets.com/process_changepet.phtml?new_active_pet=${petName}`, { method: 'POST' });
    document.querySelector('div.nav-pet-menu-icon__2020').style.backgroundImage = `url(//pets.neopets.com/cp/${sci}/1/1.png)`;
    document.querySelector('div#navProfilePet__2020').style.backgroundImage = `url(//pets.neopets.com/cp/${sci}/1/4.png)`;
    document.querySelector('div.nav-profile-dropdown-text a.profile-dropdown-link').innerHTML = petName;
    document.querySelector('div.nav-profile-dropdown-text a.profile-dropdown-link').setAttribute('href', `https://www.neopets.com/petlookup.phtml?pet=${petName}`);
};

function getActivePet() {
    return document.querySelector('.profile-dropdown-link').innerText;
}

async function makeAndAttachStar() {
    const pets = await getPets();
    if (pets.length === 0) {
        return;
    }
    const star = document.createElement('select');
    star.classList.add('active-pet-switcher');
    const activePet = getActivePet();
    pets.forEach(({ name }) => {
       star.add(new Option(name, name, activePet === name));
    });
    star.value = activePet;
    star.addEventListener('change', async ({ target: { value }}) => await handlePetChange(value));
    document.querySelector('.nav-pet-menu-icon__2020').parentNode.appendChild(star);
}

function attachStyles() {
    const styleNode = document.createElement('style');
    styleNode.type = 'text/css';
    const styles = `
        .active-pet-switcher {
            background-image: url(//images.neopets.com/themes/h5/basic/images/active-icon.svg);
            width:20px;
            height:15px;
            margin:auto auto 5px;
            background-position:center;
            background-repeat:no-repeat;
            background-size:contain;
            position: absolute;
            left: 50px;
            top: 50px;
            background-color: transparent;
            border: 0;
            -o-appearance: none;
            -ms-appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            color: transparent;
        }

        .active-pet-switcher:focus {
            outline: none;
        }

        .active-pet-switcher option {
            color: black;
        }
    `;
    if (styleNode.styleSheet) styleNode.styleSheet.cssText = styles;
    else styleNode.appendChild(document.createTextNode(styles));
    document.getElementsByTagName("head")[0].appendChild(styleNode);
}

(async function() {
    if (document.querySelector('.nav-top__2020')) {
        await savePets();
        attachStyles();
        await makeAndAttachStar();
    }
})();
